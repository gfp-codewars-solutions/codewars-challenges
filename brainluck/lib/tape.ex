defmodule Tape do
  @moduledoc """
  This module contains prototype functions for the tape component of a Brainfuck interpreter.

  It is composed of a ribbon, which stores data in cells, and a position, which acts as a pointer for the ribbon.

  Values stored in the ribbon will overflow from 255 to 0 and underflow from 0 to 255.
  """

  defstruct ribbon: %{}, pos: 0

  @type t :: %Tape{ribbon: map, pos: integer}

  @doc """
  Returns an empty tape struct.

  ## Example
      iex> Tape.new
      %Tape{pos: 0, ribbon: %{}}
  """
  @spec new() :: t
  def new, do: %Tape{}

  @doc """
  Returns a tape struct with an incremented pointer.

  ## Example
      iex> Tape.right(%Tape{})
      %Tape{pos: 1, ribbon: %{}}
  """
  @spec right(t) :: t
  def right(%{ribbon: r, pos: p}), do: %Tape{ribbon: r, pos: p + 1}

  @doc """
  Returns a tape struct with a decremented pointer.

  ## Example
      iex> Tape.left(%Tape{})
      %Tape{pos: -1, ribbon: %{}}
  """
  @spec left(t) :: t
  def left(%{ribbon: r, pos: p}), do: %Tape{ribbon: r, pos: p - 1}

  @doc """
  Returns a tape struct with the value in the `r`ibbon incremented at the `p`osition.

  ## Example
      iex> Tape.up(%Tape{})
      %Tape{pos: 0, ribbon: %{0 => 1}}

      iex> Tape.up(%Tape{pos: 0, ribbon: %{0 => 255}})
      %Tape{pos: 0, ribbon: %{0 => 0}}
  """
  @spec up(t) :: t
  def up(%{ribbon: r, pos: p}) do
    new_ribbon = case r[p] do
      255 -> %{r | p => 0}
      _ -> Map.merge(r, %{p => 1}, fn _k, v1, v2 -> v1 + v2 end)
    end

    %Tape{ribbon: new_ribbon, pos: p}
  end

  @doc """
  Returns a tape struct with the value in the `r`ibbon decremented at the `p`osition.

  ## Example
      iex> Tape.down(%Tape{})
      %Tape{pos: 0, ribbon: %{0 => 255}}
  """
  @spec down(t) :: t
  def down(%{ribbon: r, pos: p}) do
    new_ribbon = case r[p] do
      0 -> %{r | p => 255}
      nil -> Map.put(r, p, 255)
      _ -> Map.merge(r, %{p => 1}, fn _k, v1, v2 -> v1 - v2 end)
    end

    %Tape{ribbon: new_ribbon, pos: p}
  end

  @doc """
  Returns a tape struct with the value in the `r`ibbon set to `input` at the `p`osition.

  ## Example
      iex> Tape.put(%Tape{}, 71)
      %Tape{pos: 0, ribbon: %{0 => 71}}
  """
  @spec put(t, non_neg_integer) :: t
  def put(%{ribbon: r, pos: p}, input) do
    case input > 255 do
      true -> %Tape{ribbon: Map.put(r, p, input - (div(input, 255) * 255)), pos: p}
      _ -> %Tape{ribbon: Map.put(r, p, input), pos: p}
    end
  end

  @doc """
  Returns a single character string regarding the value in the `r`ibbon at the `p`osition.

  ## Example
      iex> Tape.read(%Tape{ribbon: %{0 => 104}})
      "h"
  """
  @spec read(t) :: String.t
  def read(%{ribbon: r, pos: p}) do
    case r[p] do
      nil -> <<0>>
      x -> <<x>>
    end
  end
end
