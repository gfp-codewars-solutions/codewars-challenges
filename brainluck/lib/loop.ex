defmodule Loop do
  @moduledoc """
  Functions for working with loops in the Brainfuck program.
  """

  @doc """
  Returns a boolean regarding whether or not to loop again.

  ## Example
      iex> Loop.go_back?(%Tape{ribbon: %{0 => 0}, pos: 0})
      false
  """
  @spec go_back?(Tape.t) :: boolean
  def go_back?(%{ribbon: r, pos: p}), do: r[p] not in [0, nil]

  @doc """
  Returns a boolean regarding whether or not to skip the loop.

  ## Example
      iex> Loop.skip_loop?(%Tape{ribbon: %{0 => 0}, pos: 0})
      true
  """
  @spec skip_loop?(Tape.t) :: boolean
  def skip_loop?(%{ribbon: r, pos: p}), do: r[p] in [0, nil]

  @doc """
  Returns the position of the end of the loop + 1.

  ## Example
      iex> Loop.ends_at("+[++[-->>]+]--<<", 1)
      12
  """
  @spec ends_at(String.t, non_neg_integer) :: non_neg_integer
  def ends_at(program, position) do
    program
    |> String.graphemes
    |> Enum.with_index(1)
    |> Enum.slice(position + 1, String.length(program))
    |> Enum.reduce_while(0, fn {char, pos}, nest_count ->
      case {char, nest_count} do
        {"]", 0} -> {:halt, pos}
        {"]", _} -> {:cont, nest_count - 1}
        {"[", _} -> {:cont, nest_count + 1}
        _ -> {:cont, nest_count}
      end
    end)
  end

  @doc """
  Returns the position of the beginning of the loop + 1

  ## Example
      iex> Loop.starts_at("+[++[-->>]+]--<<", 11)
      2
  """
  @spec starts_at(String.t, non_neg_integer) :: non_neg_integer
  def starts_at(program, position) do
    program
    |> String.graphemes
    |> Enum.with_index(1)
    |> Enum.slice(0, position - 1)
    |> Enum.reverse
    |> Enum.reduce_while(0, fn {char, pos}, nest_count ->
      case {char, nest_count} do
        {"[", 0} -> {:halt, pos}
        {"[", _} -> {:cont, nest_count - 1}
        {"]", _} -> {:cont, nest_count + 1}
        _ -> {:cont, nest_count}
      end
    end)
  end
end
