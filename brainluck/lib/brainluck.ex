defmodule Brainluck do
  @moduledoc """
  Functions related to interpreting a Brainfuck program.
  """

  @doc """
  Entrypoint for running a Brainfuck program.
      iex> Brainluck.brain_luck("", "")
      ""
  """
  @spec brain_luck(String.t, String.t) :: String.t
  def brain_luck(program, inputs) do
    get_next(%Program{code: program}, inputs, "", %Tape{})
  end

  def get_next(prog, inp, outp, tape) do
    {command, state} = Program.next(prog)
    run_next(command, state, inp, outp, tape)
  end

  @doc """
  Runs the next Brainfuck instruction.
  """
  @spec run_next(String.t, Program.t, String.t, String.t, Tape.t) :: String.t
  def run_next(nil, _prog, _inp, outp, _tape) do
    outp
  end

  def run_next(command, prog, inp, outp, tape) when command in ["+", "-", ">", "<"] do
    case command do
      "+" -> get_next(prog, inp, outp, Tape.up(tape))
      "-" -> get_next(prog, inp, outp, Tape.down(tape))
      ">" -> get_next(prog, inp, outp, Tape.right(tape))
      "<" -> get_next(prog, inp, outp, Tape.left(tape))
    end
  end

  def run_next(",", prog, <<v, inp :: binary>>, outp, tape) do
    get_next(prog, inp, outp, Tape.put(tape, v))
  end

  def run_next(".", prog, inp, outp, tape) do
    get_next(prog, inp, outp <> Tape.read(tape), tape)
  end

  def run_next("[", prog, inp, outp, tape) do
    case Loop.skip_loop?(tape) do
      true -> get_next(Program.move_to(prog, Loop.ends_at(prog.code, prog.pos)), inp, outp, tape)
      false -> get_next(prog, inp, outp, tape)
    end
  end

  def run_next("]", prog, inp, outp, tape) do
    case Loop.go_back?(tape) do
      true -> get_next(Program.move_to(prog, Loop.starts_at(prog.code, prog.pos)), inp, outp, tape)
      false -> get_next(prog, inp, outp, tape)
    end
  end
end
