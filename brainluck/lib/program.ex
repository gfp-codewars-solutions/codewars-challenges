defmodule Program do
  @moduledoc """
  Module for working with a Brainfuck program and it's pointer.
  """

  defstruct code: "", pos: 0

  @type t :: %Program{code: String.t, pos: 0}

  @doc """
  Returns a two item tuple consisting of an instruction and updated Program struct.
  Used for advancing to the next instruction of a Brainfuck program.

  ## Example
      iex> Program.next(%Program{code: "+-><", pos: 1})
      {"-", %Program{code: "+-><", pos: 2}}
  """
  @spec next(t) :: String.t
  def next(state) do
    {String.at(state.code, state.pos), %Program{state | pos: state.pos + 1}}
  end

  @doc """
  Returns a Program struct with the pointer set at the given position.

  ## Example
      iex> Program.move_to(%Program{code: "", pos: 1}, 49)
      %Program{code: "", pos: 49}
  """
  @spec move_to(t, non_neg_integer) :: t
  def move_to(p, pos) do
    %Program{p | pos: pos}
  end
end
