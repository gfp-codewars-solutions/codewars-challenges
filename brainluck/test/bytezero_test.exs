defmodule BytezeroTest do
  use ExUnit.Case

  test "echo until byte 0 is encoutered" do
    assert Brainluck.brain_luck(",[.[-],]", "Codewars" <> << 0 >>) == "Codewars"
  end
end
