defmodule BytemaxTest do
  use ExUnit.Case

  test "echo until byte 255 is encoutered" do
    assert Brainluck.brain_luck(",+[-.,+]", "Codewars" <> << 255 >>) == "Codewars"
  end
end
