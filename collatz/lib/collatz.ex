defmodule Collatz do
  @moduledoc """
  https://www.codewars.com/kata/collatz/train/elixir
  """
  require Integer

  @doc """
  Returns a collatz sequence starting from `n` where `n` is any positive integer.

  ## Examples

      iex> Collatz.collatz(3)
      "3->10->5->16->8->4->2->1"

  """
  @spec collatz(pos_integer) :: String.t
  def collatz(n) do
    n
    |> Stream.iterate(&(collatz_step &1))
    |> Stream.take_while(&(&1 != 1))
    |> Enum.to_list
    |> Kernel.++([1])
    |> Enum.join("->")
  end

  @spec collatz(pos_integer) :: pos_integer
  defp collatz_step(n) do
    case Integer.is_odd(n) do
      true -> 3 * n + 1
      false -> div(n, 2)
    end
  end

end
