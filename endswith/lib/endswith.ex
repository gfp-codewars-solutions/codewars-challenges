defmodule EndsWith do
  @moduledoc """
  https://www.codewars.com/kata/string-ends-with/train/elixir
  """

  @doc """
  Checks if a given string ends with a given substring.
  Returns `true` on success and `false` otherwise.

  ## Examples

      iex> EndsWith.solution("abcd", "bcd")
      true

      iex> EndsWith.solution("aslkjdfasd", "asjkd")
      false

  """
  @spec solution(String.t, String.t) :: boolean
  def solution(s, ending) do
    String.ends_with?(s, ending)
  end
end
