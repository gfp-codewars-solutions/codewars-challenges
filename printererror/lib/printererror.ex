defmodule Printererror do
  @moduledoc """
  https://www.codewars.com/kata/printer-errors/train/elixir
  """

  @doc """
  Give it a string. It will return a fraction as errors/length.

  ## Examples

      iex> Printererror.printer_error("aaaz")
      "1/4"

      iex> Printererror.printer_error("aaa")
      "0/3"

  """
  @spec printer_error(String.t) :: String.t
  def printer_error(s) do
    {error_count, total_length} = s
    |> String.graphemes
    |> Enum.reduce({0, 0}, fn(x, {e, l}) -> {e + (x > "m" && 1 || 0), l + 1} end)
    "#{error_count}/#{total_length}"
  end
end
