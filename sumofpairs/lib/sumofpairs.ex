defmodule SumOfPairs do
  @moduledoc """
  https://www.codewars.com/kata/54d81488b981293527000c8f/train/elixir
  """

  @doc """
  Finds the first pair of ints as judged by the index of the second value.
  ## Examples:
      iex> sum_pairs([10, 5, 2, 3, 7, 5 ], 10)
      {3, 7}
  """
  @spec sum_pairs([integer, ...], integer) :: {integer, integer} | nil
  def sum_pairs([h | t], sum), do: sum_pairs(t, sum, %{sum - h => h})

  @spec sum_pairs([], integer, %{integer => integer}) :: nil
  defp sum_pairs([], _, _), do: nil

  @spec sum_pairs([integer, ...], integer, %{integer => integer}) :: {integer, integer}
  defp sum_pairs([h | t], sum, addend_map) do
    case addend_map[h] do
      nil -> sum_pairs(t, sum, Enum.into(addend_map, %{sum - h => h}))
      _ -> {addend_map[h], h}
    end
  end

end
