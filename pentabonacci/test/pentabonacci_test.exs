defmodule PentabonacciTest do
  use ExUnit.Case
  doctest Pentabonacci

  test "45 levels should have 15 odd numbers" do
    {n, ans} = {45, 15}
    assert Pentabonacci.count_odd_pentaFib(n) == ans
  end

  test "68 levels should have 23 odd numbers" do
    {n, ans} = {68, 23}
    assert Pentabonacci.count_odd_pentaFib(n) == ans
  end

  test "76 levels should have 25 odd numbers" do
    {n, ans} = {76, 25}
    assert Pentabonacci.count_odd_pentaFib(n) == ans
  end

  test "100 levels should have 33 odd numbers" do
    {n, ans} = {100, 33}
    assert Pentabonacci.count_odd_pentaFib(n) == ans
  end

  test "121 levels should have 40 odd numbers" do
    {n, ans} = {121, 40}
    assert Pentabonacci.count_odd_pentaFib(n) == ans
  end

  test "pentabonacci generation is correct" do
    ans = [1, 1, 2, 4, 8, 16, 31, 61, 120, 236, 464, 912, 1793, 3525, 6930]
    assert Pentabonacci.pentafib(15) == ans
  end
end
