defmodule Pentabonacci do
  @moduledoc """
  https://www.codewars.com/kata/pentabonacci/train/elixir
  """
  require Integer

  @doc """
  Counts the amount of odd integers in an arbitrary length Pentabonacci sequence.

  ## Examples

      iex> Pentabonacci.count_odd_pentaFib(45)
      15

  """
  @spec count_odd_pentaFib(pos_integer) :: number
  def count_odd_pentaFib(n) do
    n
    |> pentafib
    |> Enum.dedup
    |> Enum.reduce(0, &(Integer.is_odd(&1) && &2+1 || &2))
  end

  @doc """
  Generates a Pentabonacci sequence up up to `n` length.

  ## Examples

      iex> Pentabonacci.pentafib(6)
      [1, 1, 2, 4, 8, 16]
  """
  @spec pentafib(pos_integer) :: [pos_integer, ...]
  def pentafib(n) do
    {1, 1, 2, 4, 8}
    |> Stream.unfold(fn({a, b, c, d, e}) -> {a, {b, c, d, e, a+b+c+d+e}} end)
    |> Enum.take(n)
  end
end
