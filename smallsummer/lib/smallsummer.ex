defmodule SmallSummer do
  @moduledoc """
  https://www.codewars.com/kata/sum-of-two-lowest-positive-integers/train/elixir
  """

  @doc """
  Returns the sum of the two smallest numbers in a list.

  ## Examples

      iex> SmallSummer.sum_two_smallest_numbers([1, 2, 3, 4, 5, 1])
      2

  """
  @spec sum_two_smallest_numbers([number, ...]) :: number
  def sum_two_smallest_numbers(numbers) do
    numbers
    |> Enum.sort
    |> Enum.take(2)
    |> Enum.sum
  end
end
