defmodule TestGreeter do
  use ExUnit.Case
  doctest Greeter
  import Greeter, only: [greet: 1]

  test "english should return Welcome" do
    assert greet("english") == "Welcome"
  end

  test "dutch should return Welkom" do
    assert greet("dutch") == "Welkom"
  end

  test "IP_ADDRESS_INVALID should return Welcome" do
    assert greet("IP_ADDRESS_INVALID") == "Welcome"
  end
end
