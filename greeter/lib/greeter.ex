defmodule Greeter do
  @moduledoc """
  https://www.codewars.com/kata/577ff15ad648a14b780000e7/train/elixir
  """

  @languages %{
    "english" => "Welcome",
    "czech" => "Vitejte",
    "danish" => "Velkomst",
    "dutch" => "Welkom",
    "estonian" => "Tere tulemast",
    "finnish" => "Tervetuloa",
    "flemish" => "Welgekomen",
    "french" => "Bienvenue",
    "german" => "Willkommen",
    "irish" => "Failte",
    "italian" => "Benvenuto",
    "latvian" => "Gaidits",
    "lithuanian" => "Laukiamas",
    "polish" => "Witamy",
    "spanish" => "Bienvenido",
    "swedish" => "Valkommen",
    "welsh" => "Croeso"
  }

  @doc """
  Give it a language and it returns a greeting.

  ## Examples:
      iex> Greeter.greet("french")
      "Bienvenue"
  """
  @spec greet(String.t) :: String.t
  def greet(language) do
    case Map.fetch(@languages, language) do
      {:ok, greeting} -> greeting
      :error -> "Welcome"
    end
  end
end
