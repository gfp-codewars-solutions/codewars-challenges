defmodule TribonacciSequence do
  @moduledoc """
  https://www.codewars.com/kata/tribonacci-sequence/train/elixir
  """

  @doc """
  Returns a sequence starting with the numbers in the first argument.
  Sums the last three items in the sequence to find the next item in the sequence.
  Second argument determines how many arguments to return.

  ## Examples

      iex> TribonacciSequence.tribonacci({1, 2, 3}, 4)
      [1, 2, 3, 6]

  """
  @spec tribonacci({number, number, number}, non_neg_integer) :: [number, ...]
  def tribonacci(sig, n) do
    sig
    |> Stream.unfold(fn({a, b, c}) -> {a, {b, c, a+b+c}} end)
    |> Enum.take(n)
  end

end
