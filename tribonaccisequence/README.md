# TribonacciSequence

[CodeWars Link](https://www.codewars.com/kata/tribonacci-sequence/train/elixir)

## Learned

+ If I'm generating a list of dynamic length, I should first consider using the Stream module.
