defmodule Opposites do
  @moduledoc """
  https://www.codewars.com/kata/opposites-attract/train/elixir
  """
  require Integer

  @doc """
  Returns true if one number is even and the other is odd

  ## Examples

      iex> Opposites.inlove?(1,4)
      true

  """
  @spec inlove?(integer, integer) :: boolean
  def inlove?(x, y), do: Integer.is_even(x) != Integer.is_even(y)
end
