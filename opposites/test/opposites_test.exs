defmodule TestOpposites do
  use ExUnit.Case
  doctest Opposites
  import Opposites, only: [inlove?: 2]

  test "odd, even is true" do
    assert inlove?(1,4) == true
  end

  test "even, even is false" do
    assert inlove?(2,2) == false
  end

  test "even, odd is true" do
    assert inlove?(0,1) == true
  end

  test "zero, zero is false" do
    assert inlove?(0,0) == false
  end
end
