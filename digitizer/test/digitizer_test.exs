defmodule DigitizerTest do
  use ExUnit.Case
  doctest Digitizer

  defmodule TestDigitizer do
  use ExUnit.Case
  import Digitizer, only: [digitize: 1]

  test "reverse 35231" do
    assert digitize(35231) == [1,3,2,5,3]
  end

  test "reverse 1" do
    assert digitize(1) == [1]
  end

  test "reverse 23987478923489723987" do
    assert digitize(23987478923489723987) == [7, 8, 9, 3, 2, 7, 9, 8, 4, 3, 2, 9, 8, 7, 4, 7, 8, 9, 3, 2]
  end
end
end
