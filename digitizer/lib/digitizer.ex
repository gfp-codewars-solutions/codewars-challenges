defmodule Digitizer do
  @moduledoc """
  https://www.codewars.com/kata/577ff15ad648a14b780000e7/train/elixir
  """

  @doc """
  Returns an int as a charlist in reverse order

  ## Examples

      iex> Digitizer.digitize(123)
      [3, 2, 1]

  """
  @spec digitize(pos_integer) :: [pos_integer, ...]
  def digitize(number), do: number |> Integer.digits |> Enum.reverse
end
