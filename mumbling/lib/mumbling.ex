defmodule Mumbling do
  @moduledoc """
  http://www.codewars.com/kata/5667e8f4e3f572a8f2000039/train/elixir
  """

  @doc """
  Multiplies each letter by it's 1-based index, placing a dash in between each.

  ## Examples

      iex> Mumbling.accum("abcd")
      "A-Bb-Ccc-Dddd"

  """
  @spec accum(String.t) :: String.t
  def accum(s) do
    s
    |> String.upcase
    |> String.graphemes
    |> Enum.reduce({[], 1}, fn(x, {a, f}) -> {[format_string(x, f) | a], f + 1} end)
    |> elem(0)
    |> Enum.reverse
    |> Enum.join("-")
  end

  @spec format_string(String.t, pos_integer) :: String.t
  defp format_string(s, f) when f == 1 do
    String.upcase(s)
  end

  @spec format_string(String.t, pos_integer) :: String.t
  defp format_string(s, f) do
    String.upcase(s) <> String.duplicate(String.downcase(s), f - 1)
  end
end
