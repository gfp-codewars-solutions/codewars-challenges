# Mumbling

[Codewars Link](http://www.codewars.com/kata/5667e8f4e3f572a8f2000039/train/elixir)

## What I learned

+ `Enum.with_index/2`
+ `String.capitalize/1`
+ `Enum.map_join/3`
